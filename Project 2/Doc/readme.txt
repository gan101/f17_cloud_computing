This text documents guides the user on how do we run the programs on shared memory

with explanations of code, which file contains what, and the commands needed to compile and execute the different scenarios

SHARED MEMORY
a) Files Description
The source code under "src_shared_memory" directory contains 6 java files under src/com/iit/cs553/
-Chunck.java: 
 This file contains the the list of Record objects and this object represents one chunck of data, which is calulcate dbased on the ram size and threads.
-FileMerge.java:
 This file contains the logic required to merge the any two sorted files and create a bigger sorted file.
-Record.java:
 This file Represent one record from the input file.It is having bytes of data.
-SharedMemory.java
 This class is the driver class for executing shared memory.
-SortData.java
 This file is used to logically sort the data based on merge sort algorithm for each chunck in each thread.
-Utilty.java
 This files is used for all helper realted functions.This is internally used by above 5 files. 

b) Steps to compile the Shared memory
	1.Change directory to "src_shared_memory" directory
	cd src_shared_memory
	
	2.Compile all java files
	 javac com/iit/cs553/*.java
	 
	3.Create a jar file from the above class files 
	jar cf sharedmemory.jar *.class
	
	4.Execute the jar using 
	jar sharedmemory.jar SharedMemory
 
 c)Commands to execute shared memory
	1. Install java
	cd ~
	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get install ssh
	sudo apt-get install default-jdk
	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
	
	2.change directory to "src_shared_memory" directory
	cd src_shared_memory
	
	3.for configuration 1 shared memory tera sort execution,we need to execute below script.
	shared_memory_config1.sh

	4.for configuration 2 shared memory tera sort execution, we need to execute
	shared_memory_config2.sh
	